@extends('layouts.admin')

@section('content')
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Manage Businesses</h1>
<div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="#" class="btn btn-sm btn-success shadow-sm float-right" data-toggle="modal" data-target="#businessAddEditModal">
                    <i class="fas fa-plus fa-sm text-white-50"></i> Add New</a>

                <a href="#" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Export</a>

                <hr class="sidebar-divider">
                <form class="form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
                </form>                
            </div>
            <div class="card-body">
               
              <div class="table-responsive">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>Demo Text</td>
                        <td>Active</td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#businessAddEditModal" class="btn btn-info btn-circle btn-sm">
                              <i class="fas fa-pen"></i>
                            </a>
                            <a href="#" class="btn btn-danger btn-circle btn-sm">
                              <i class="fas fa-trash"></i>
                            </a>                            
                        </td>
                        </tr>
                    </tbody>
                </table> 
              </div>
            </div>
          </div>
</div>
@endsection

@push('page_modals')
 @include('modals.business_add_edit')
@endpush